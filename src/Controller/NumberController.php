<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\numbersapiService;

class NumberController extends AbstractController
{
    /**
     * @Route("/number", name="number_get", methods={"GET"})
     */
    public function index()
    {
        return $this->render('number/index.html.twig');
    }

    /**
     * @Route("/number", name="number_post", methods={"POST"})
     * @param Request $req
     * @param numbersapiService $nbrAPI
     * @return Response
     */
    public function process(Request $req, numbersapiService $nbrAPI)
    {
        $datas = $req->request->all();
        return $this->render('number/index.html.twig', [
            "nombre"        => $datas["nombre_choisi"],
            "resultat_api"  => $nbrAPI->getNumberFact($datas["nombre_choisi"])
        ]);
    }
}
